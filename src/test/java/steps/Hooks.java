package steps;

import com.yalla.selenium.api.base.SeleniumBase;

import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;

public class Hooks extends SeleniumBase{
	//For POM implementation with cucumber, we need to add all Before methods like in order = suite,test,class,method
	@Before
	public void beforescenario(Scenario sc) {
		System.out.println(sc.getName());
		System.out.println(sc.getId());	
		startReport();
		test = extent.createTest(sc.getName(), sc.getId());
	    test.assignAuthor("Sreedar");
	    test.assignCategory("Smoke");
	    startApp("chrome", "http://leaftaps.com/opentaps");
		
	}
	
	//For POM implementation with cucumber, we need to add all after methods like in order = method,class,test,suite
	
	@After
	public void afterscenario(Scenario sc) {
		System.out.println(sc.getStatus());
		stopReport();
		close();
		
			
		
	}
	

}
