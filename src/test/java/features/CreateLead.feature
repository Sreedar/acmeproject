Feature: Create Lead for LeafTap Application
#Background:
#Given open the browser
#And maximize the browser
#And enter the url

@smoke
Scenario Outline: Positive Create Lead Flow

And enter the username as <uname>
And enter the password as <pwd> 
And click on login button
And click on CRMSFA
And click on Create Lead
And enter the Company name as <cmpnyname>
And enter the firstname as <firstname>
And enter the lastname as <lastname>
When click on Create Lead button
Then verify create lead success

Examples:
|uname|pwd|cmpnyname|firstname|lastname|
|DemoSalesManager|crmsfa|Amazon|Sreedar|Ramasamy|
|DemoSalesManager|crmsfa|Cognizant|Vasanth|Kumar|

#@reg
#Scenario: Negative Create Lead Flow
#
#And enter the username as DemoSalesmanager 
#And enter the password as crmsfa 
#And click on login button
#And click on CRMSFA
#And click on Create Lead
#And enter the Company nameas TestLeaf
#And enter the firstnameas Sreedar
#And enter the lastnameas R
#When click on Create Lead button
#Then verify create lead success



