package acmeproject;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import utils.DataLibrary;

public class Vendorsearch {

	@Test(dataProvider="data")
	public static void day(String un, String pw, String tax){
		System.setProperty("webdriver.chrome.driver", "./Drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.get("https://acme-test.uipath.com/account/login");
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		
		//driver.findElementById("email").sendKeys("Sreedarmca@gmail.com");
		
		WebElement email = driver.findElementById("email");
		email.sendKeys(un);
		WebElement pwd = driver.findElementById("password");
		pwd.sendKeys(pw);
		driver.findElementById("buttonLogin").click();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.findElementByXPath("//*[text()=' Vendors']").click();
		WebElement srch = driver.findElementByXPath("//a[text()=\"Search for Vendor\"]");
		Actions action = new Actions(driver);
		action.moveToElement(srch).perform();
		srch.click();
		WebElement taxid = driver.findElementById("vendorTaxID");
		taxid.sendKeys(tax);
		driver.findElementById("buttonSearch").click();
		System.out.println("Tax Id Searched Successfully");
		

	}
	
	@DataProvider(name="data")
	
	public Object[][] getdata() throws Throwable{
		
		
		return DataLibrary.readExcelData("TC005");
	}

}
