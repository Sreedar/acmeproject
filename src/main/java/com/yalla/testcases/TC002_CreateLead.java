package com.yalla.testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.yalla.pages.LoginPage;
import com.yalla.testng.api.base.Annotations;

public class TC002_CreateLead extends Annotations{
	
	@BeforeTest
	public void setData() {
		testcaseName = "TC002_LoginAndLogout";
		testcaseDec = "Login into leaftaps";
		author = "Gayatri";
		category = "smoke";
		excelFileName = "TC002";
	} 

	@Test(dataProvider="fetchData") 
	public void createLead(String uName, String pwd,String co , String fn , String ln) {
		new LoginPage()
		.enterUserName(uName)
		.enterPassWord(pwd) 
		.clickLogin()
		.clickCrmsfa()
		.clickLead()
		.clickCreateLead()
		.enterCompanyName(co)
		.enterFirstName(fn)
		.enterLastName(ln)
		.clickCreateLeadButton();
		
	
	
		
		
		
		
		/*LoginPage lp = new  LoginPage();
		lp.enterUserName();
		lp.enterPassWord();*/
		
	}
	
}






