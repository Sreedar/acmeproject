package com.yalla.testcases;


import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.yalla.pages.LoginPage;
import com.yalla.testng.api.base.Annotations;

public class TC003_EditLead extends Annotations{
	
	@BeforeTest
	public void setData() {
		testcaseName = "TC003 Edit Lead TestCase";
		testcaseDec = "Edit Lead";
		author = "Sreedar";
		category = "Functional";
		excelFileName = "TC003";
	} 

	@Test(dataProvider="fetchData") 
	public void EditLead(String uName, String pwd,String co , String fn , String ln,String uc , String ufn , String uln) {
		new LoginPage()
		.enterUserName(uName)
		.enterPassWord(pwd) 
		.clickLogin()
		.clickCrmsfa()
		.clickLead()
		.clickCreateLead()
		.enterCompanyName(co)
		.enterFirstName(fn)
		.enterLastName(ln)
		.clickCreateLeadButton()
		.clickEditLead()
		.updateCompanyName(uc)
		.updateFirstName(ufn)
		.updateLastName(uln)
		.clickUpdateLeadButton();
	    	
	
		
	
		
		
	
	
		
		
		
		
		/*LoginPage lp = new  LoginPage();
		lp.enterUserName();
		lp.enterPassWord();*/
		
	}

	private void clickEditLead() {
		// TODO Auto-generated method stub
		
	}
	
}






