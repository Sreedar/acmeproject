package com.yalla.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.yalla.testng.api.base.Annotations;

public class FindLeadsPage extends Annotations{ 

	public FindLeadsPage() {
       PageFactory.initElements(driver, this);
	} 

	@FindBy(how=How.ID_OR_NAME, using="id")  WebElement eleLeadId;
	@FindBy(how=How.LINK_TEXT, using="Find Leads")  WebElement eleFindLeadsbutton;
	@FindBy(how=How.XPATH, using="//*[@class='x-grid3-cell-inner x-grid3-col-partyId']") WebElement eleclickfirstlead;
	
	public FindLeadsPage enterLeadID(String data) {
		
		clearAndType(eleLeadId, data);  
		return this; 
	}
	public ViewLeadsPage clickFindLeadsbutton() {
		
        click(eleFindLeadsbutton);  
        return new ViewLeadsPage();
	}
		
	public ViewLeadsPage clickFirstLead() {
		
          click(eleclickfirstlead);  
          return new ViewLeadsPage();
	}
	
	public MergeLeadsPage clickFromFirstLead() {
		clickWithNoSnap(eleclickfirstlead);
        switchToWindow(0);
        return new MergeLeadsPage();
	}
	public MergeLeadsPage clickToFirstLead() {
		clickWithNoSnap(eleclickfirstlead);
        switchToWindow(0);
        return new MergeLeadsPage();
	}
	
	
}