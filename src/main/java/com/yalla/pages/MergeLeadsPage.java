package com.yalla.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.yalla.testng.api.base.Annotations;

public class MergeLeadsPage extends Annotations{ 

	public MergeLeadsPage() {
       PageFactory.initElements(driver, this);
	} 

	@FindBy(how=How.XPATH, using="(//img)[9]") WebElement eleFromLead;
	@FindBy(how=How.XPATH, using="(//img)[10]") WebElement eleToLead;
	@FindBy(how=How.LINK_TEXT, using="Merge") WebElement eleClickMerge;
	
	public FindLeadsPage  clickFromLead() {
		
          click(eleFromLead); 
          switchToWindow(1);
          return new FindLeadsPage();
	}
	public  FindLeadsPage clickToLead() {
		
        click(eleToLead);  
        return new FindLeadsPage();
	}

	
	public ViewLeadsPage clickMergeLead() {
			clickWithNoSnap(eleClickMerge);
			switchToAlert();
			getAlertText();
			acceptAlert();
            return new ViewLeadsPage();
	}


}







