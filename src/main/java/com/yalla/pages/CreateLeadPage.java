package com.yalla.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.yalla.testng.api.base.Annotations;

import cucumber.api.java.en.And;
import cucumber.api.java.en.When;

public class CreateLeadPage extends Annotations {
		
		public CreateLeadPage() {
			PageFactory.initElements(driver, this);
		}
			
		@FindBy(how=How.ID_OR_NAME, using="createLeadForm_companyName")  WebElement eleCmpnyName;
		@FindBy(how=How.ID, using="createLeadForm_firstName")  WebElement eleFirstName;
		@FindBy(how=How.ID, using="createLeadForm_lastName") WebElement eleLastName;
		@FindBy(how=How.NAME, using="submitButton") WebElement eleSubmit;
		
		@And("enter the Company name as (.*)")
		public CreateLeadPage enterCompanyName(String data) {
			
			clearAndType(eleCmpnyName, data);  
			return this; 
		}
		@And("enter the firstname as (.*)")
		public CreateLeadPage enterFirstName(String data) {
			//WebElement elePassWord = locateElement("id", "password");
			clearAndType(eleFirstName, data); 
			return this; 
		}
		@And("enter the lastname as (.*)")
		public CreateLeadPage enterLastName(String data) {
			clearAndType(eleLastName, data); 
			return this; 
		}
		@When("click on Create Lead button")	
		public ViewLeadsPage clickCreateLeadButton() {
			
	          click(eleSubmit);  
	          return new ViewLeadsPage();
		}

	}


