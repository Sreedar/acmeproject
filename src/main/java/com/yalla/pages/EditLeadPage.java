package com.yalla.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.yalla.testng.api.base.Annotations;

public class EditLeadPage extends Annotations{ 

	public EditLeadPage() {
       PageFactory.initElements(driver, this);
	} 

	@FindBy(how=How.ID_OR_NAME, using="updateLeadForm_companyName")  WebElement eleCmpnyName;
	@FindBy(how=How.ID, using="updateLeadForm_firstName")  WebElement eleFirstName;
	@FindBy(how=How.ID, using="updateLeadForm_lastName") WebElement eleLastName;
	@FindBy(how=How.XPATH, using="//*[@value=\"Update\"]") WebElement eleUpdate;
	
	public EditLeadPage updateCompanyName(String data) {
		
		append(eleCmpnyName, data); 
		return this; 
	}
	
	public EditLeadPage updateFirstName(String data) {
		append(eleFirstName, data);
		return this; 
	}
	
	public EditLeadPage updateLastName(String data) {
		
		append(eleLastName, data);
		return this; 
	}

	public ViewLeadsPage clickUpdateLeadButton() {
		
          click(eleUpdate);  
          return new ViewLeadsPage();
	}
	
	
	}








