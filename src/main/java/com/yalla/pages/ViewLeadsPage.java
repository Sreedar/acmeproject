package com.yalla.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.IFactoryAnnotation;

import com.yalla.testng.api.base.Annotations;

import cucumber.api.java.en.Then;

public class ViewLeadsPage extends Annotations {
		
		public ViewLeadsPage() {
			PageFactory.initElements(driver, this);
		}
			
		@FindBy(how=How.LINK_TEXT, using="Edit") WebElement eleEditLead;
		
		@FindBy(how=How.ID_OR_NAME, using="createLeadForm_companyName")  WebElement eleCmpnyName;
		@FindBy(how=How.ID, using="createLeadForm_firstName")  WebElement eleFirstName;
		@FindBy(how=How.ID, using="createLeadForm_lastName") WebElement eleLastName;
		@FindBy(how=How.NAME, using="submitButton") WebElement eleSubmit;
		
        public EditLeadPage verifyInfo(String data) {
        	
        	if (verifyExactText(eleCmpnyName, data)&& verifyExactText(eleFirstName, data)&& verifyExactText(eleCmpnyName, data) ) {
        		click(eleEditLead); 
			} 
        	
        	else {
				System.out.println("No Matching Records - Failed");
			}
                
        	return new EditLeadPage();
        }
		
	/*	public ViewLeadsPage verifyCompanyName(String data) {
			verifyExactText(eleCmpnyName, data);
			return this; 
		}
		
		public ViewLeadsPage verifyFirstName(String data) {
			//WebElement elePassWord = locateElement("id", "password");
			verifyExactText(eleFirstName, data);
			return this; 
		}
		
		public ViewLeadsPage verifyLastName(String data) {
			verifyExactText(eleLastName, data);
			return this; 
		}
		*/
        @Then("verify create lead success")
        public void verify() {
        	System.out.println(" Sucessfully executed POM in  Cucumber");
        }
		public EditLeadPage clickEditLead() {
	          click(eleEditLead);  
	          return new EditLeadPage();
		}
}
		
		
	    
	
		

	


